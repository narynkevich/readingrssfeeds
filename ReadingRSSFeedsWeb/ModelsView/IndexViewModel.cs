﻿using CommonLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadingRSSFeedsWeb.ModelsView
{
    public class IndexViewModel
    {
        public IEnumerable<FeedItem> FeedItems { get; set; }
        public PageViewModel PageViewModel { get; set; }
        public FilterViewModel FilterViewModel { get; set; }
        public SortViewModel SortViewModel { get; set; }
    }


}
