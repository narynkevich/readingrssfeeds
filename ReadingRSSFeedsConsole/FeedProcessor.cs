﻿using CodeHollow.FeedReader;
using CommonLib;
using System;
using System.Data;
using System.Linq;
using System.Threading;

namespace ReadingRSSFeedsConsole
{
    public class FeedProcessor
    {
        string _Uri;
        private bool _started;
        private readonly RSSReadingDbContext db;

      

        public FeedProcessor(RSSReadingDbContext context)
        {
            db = context;
        }

        public void Start()
        {
            if (!_started)
            {
                _started = true;
                DoWork();
            }
        }

        private void DoWork()
        {
            if (!db.Feeds.Any())
            {
                Console.WriteLine("В таблице Feed БД не найдено ни одного RSS адреса!");
                return;
            }


            while (true)
            {
                try
                {
                    // Для каждого адреса RSS ленты:
                    var feedsDb = db.Feeds.ToList();
                    foreach (var feedDb in feedsDb)
                    {
                        _Uri = feedDb.Link;
                        
                        var urlsTask = FeedReader.GetFeedUrlsFromUrlAsync(_Uri);
                        var urls = urlsTask.Result;

                        string feedUrl;
                        if (urls == null || urls.Count() < 1)
                            feedUrl = _Uri;
                        else if (urls.Count() == 1)
                            feedUrl = urls.First().Url;
                        else if (urls.Count() == 2) // if 2 urls, then its usually a feed and a comments feed, so take the first per default
                            feedUrl = urls.First().Url;
                        else
                        {
                            int i = 1;
                            Console.WriteLine("Found multiple feed, please choose:");
                            foreach (var feedurl in urls)
                            {
                                Console.WriteLine($"{i++.ToString()} - {feedurl.Title} - {feedurl.Url}");
                            }
                            var input = Console.ReadLine();

                            if (!int.TryParse(input, out int index) || index < 1 || index > urls.Count())
                            {
                                Console.WriteLine("Wrong input. Press key to exit");
                                Console.ReadKey();
                                return;
                            }
                            feedUrl = urls.ElementAt(index).Url;
                        }

                        var readerTask = FeedReader.ReadAsync(feedUrl);
                        readerTask.ConfigureAwait(false);

                        var feed = FeedReader.Read(feedUrl);

                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.WriteLine("URL ленты:\t\t" + _Uri);
                        Console.WriteLine("Заголовок ленты:\t" + feed.Title);
                        Console.WriteLine("---------------------------------------------------------------------------");

                        
                        var countAllItems = readerTask.Result.Items.Count; // Общее количество новостей
                        var countNewItems = 0; // Количество новых новостей

                        foreach (var item in readerTask.Result.Items)
                        {                           
                            // Добавление только новых новостей
                            if (!db.FeedItems.Where(fi => fi.RSSItemId == item.Id).Any())
                            {
                                countNewItems++;

                                var fi = new CommonLib.FeedItem
                                {
                                    RSSItemId = item.Id,
                                    Title = item.Title,
                                    Link = item.Link,
                                    Description = item.Description,
                                    PublishingDate = item.PublishingDate,
                                    Feed = feedDb
                                };

                                db.FeedItems.Add(fi);
                                db.SaveChanges();
                            }
                        }

                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine($"Общее количество новостей:\t {countAllItems}");
                        Console.WriteLine($"Сохранено новостей в БД:\t {countNewItems}");
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.WriteLine("---------------------------------------------------------------------------");
                        Console.WriteLine($"Дата чтения:\t\t {DateTime.Now}\n\n\n");

                        Console.ResetColor();


                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine($"An error occurred: {ex.InnerException.Message}{Environment.NewLine}{ex.InnerException.ToString()}");
                }
                finally
                {
                    Console.WriteLine("\n\nПовторное чтение новостей: через 1 минуту (для остановки нажмите \"<Ctrl> + C\").\n\n\n\n");
                }

                Thread.Sleep(TimeSpan.FromMinutes(1));

            }
        }

        /*
        if (feedUriii.Any())
        {
            string lastId = "";
            var feedUri = feedUris.First().Url;
            while (true)
            {
                var feed = await FeedReader.ReadAsync(feedUri);
                var newItems = new List<FeedItem>();
                foreach (var item in feed.Items)
                {
                    if (item.Id == lastId)
                        break;
                    newItems.Add(item);
                }
                if (feed.Items.Any())
                    lastId = feed.Items.First().Id;
                newItems.Reverse();
                foreach (var item in newItems)
                    Console.WriteLine(item.Link);

                await Task.Delay(TimeSpan.FromMinutes(1));
            }
        } */
    }
}

