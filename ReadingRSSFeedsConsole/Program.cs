﻿using System;
using System.Text;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using CommonLib;
using Microsoft.EntityFrameworkCore;

namespace ReadingRSSFeedsConsole
{
    class Program
    {
        public static IConfigurationRoot Configuration;


        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;


            string environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

/*            if (String.IsNullOrWhiteSpace(environment))
                throw new ArgumentNullException("Environment not found in ASPNETCORE_ENVIRONMENT"); */

            // Настроить конфигурацию приложения: файл настроек "appsettings.json".
            var builder = new ConfigurationBuilder()
                .SetBasePath(Path.Combine(AppContext.BaseDirectory))
                .AddJsonFile("appsettings.json", optional: true);
            if (environment == "Development")
            {
                builder
                    .AddJsonFile(
                        Path.Combine(AppContext.BaseDirectory, string.Format("..{0}..{0}..{0}", Path.DirectorySeparatorChar), $"appsettings.{environment}.json"),
                        optional: true
                    );
            }
            else if (environment != null)
            {
                builder
                    .AddJsonFile($"appsettings.{environment}.json", optional: false);
            }

            Configuration = builder.Build();


            // Получить строку подключения из файла конфигурации
            string connection = Configuration.GetConnectionString("DefaultConnection");



            // Настройка параметров и DI
            var services = new ServiceCollection();
            services.AddOptions();

            // Добавление контекста DBContext в качестве сервиса в приложение
            services.AddDbContext<RSSReadingDbContext>(options =>
                options.UseSqlServer(connection));

            services.AddTransient<FeedProcessor>();
            services.AddSingleton<SampleData>();

            services.AddSingleton<IConfiguration>(Configuration);

            var serviceProvider = services.BuildServiceProvider();

            var setSampleData = serviceProvider.GetService<SampleData>();
            setSampleData.Initialize();

            var feedProcessor = serviceProvider.GetService<FeedProcessor>();
            feedProcessor.Start();
        }
    }
}

