﻿using System.Linq;

namespace CommonLib
{
    public class SampleData
    {
        RSSReadingDbContext _context;

        public SampleData(RSSReadingDbContext context)
        {
            _context = context;
        }

        public void Initialize()
        {
            if (!_context.Feeds.Any())
            {
                _context.Feeds.AddRange(
                    new Feed
                    {
                        Link = @"https://interfax.by/news/feed/"
                    },
                    new Feed
                    {
                        Link = @"https://habr.com/ru/rss/all/all/"
                    }
                );
                _context.SaveChanges();
            }
        }
    }
}
