﻿using System;
using System.Collections.Generic;

namespace CommonLib
{
    /// <summary>
    /// RSS лента
    /// </summary>
    public class Feed
    {

        public int Id { get; set; }

        /// <summary>
        /// URL ленты
        /// </summary>
        public string Link { get; set; }


        string _host;
        /// <summary>
        /// Если имя Хоста не указано, то взять его из свойства Link
        /// </summary>
        public string Host
        {
            get
            {
                if (Link != null && _host == null)
                    return new Uri(Link).Host;
                else
                    return _host;
            }
            set
            {
                _host = value;
            }
        }

        /// <summary>
        /// Заголовок
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }


        /// <summary>
        /// URL изображения
        /// </summary>
        public int ImageUrl { get; set; }



        // Навигационные свойства
        public List<FeedItem> FeedItems { get; set; }
        public Feed()
        {
            // Host = new Uri(Link).Host;
            FeedItems = new List<FeedItem>();
        }
    }
}